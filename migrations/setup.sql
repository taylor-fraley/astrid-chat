create schema astrid;

CREATE TABLE astrid.scenes (
    scene_id INT GENERATED ALWAYS AS IDENTITY,
    setting1 VARCHAR NOT NULL,
    setting2 VARCHAR,
    start_time TIMESTAMP,
    end_time TIMESTAMP,
    PRIMARY KEY(scene_id)
);

CREATE TABLE astrid.users (
    user_id INT GENERATED ALWAYS AS IDENTITY,
    display_name VARCHAR,
    email VARCHAR,
    image_path VARCHAR,
    PRIMARY KEY(user_id)
);

CREATE TABLE astrid.messages (
    message_id INT GENERATED ALWAYS AS IDENTITY,
    scene_id INT,
    from_user_id INT,
    body TEXT,
    published TIMESTAMP,
    PRIMARY KEY(message_id),
    CONSTRAINT fk_scene
        FOREIGN KEY(scene_id)
            REFERENCES astrid.scenes(scene_id),
    CONSTRAINT fk_user
        FOREIGN KEY(from_user_id)
            REFERENCES astrid.users(user_id)
);

INSERT into astrid.scenes (
    setting1,
    setting2)
VALUES(
    'Rivertown',
    'Town Square'
);

INSERT into astrid.users (
    display_name,
    image_path,
    email)
VALUES(
    'Barlot',
    'https://cdn2.vectorstock.com/i/1000x1000/20/76/man-avatar-profile-vector-21372076.jpg',
    'ali@example.com'
);

insert into astrid.messages(scene_id, from_user_id, body)
values(1, 1, 'hello all'),
        (1, 1, 'great to be here');

create role web_anon nologin;

grant usage on schema astrid to web_anon;
grant select on astrid.scenes to web_anon;
grant select on astrid.users to web_anon;
grant select on astrid.messages to web_anon;

create role authenticator noinherit login password 'secretpass';
grant web_anon to authenticator;