CREATE TABLE scenes (
    scene_id INT GENERATED ALWAYS AS IDENTITY,
    setting1 VARCHAR NOT NULL,
    setting2 VARCHAR,
    start_time TIMESTAMP,
    end_time TIMESTAMP,
    PRIMARY KEY(scene_id)
);

CREATE TABLE users (
    user_id INT GENERATED ALWAYS AS IDENTITY,
    display_name VARCHAR,
    email VARCHAR,
    PRIMARY KEY(user_id)
);

CREATE TABLE messages (
    message_id INT GENERATED ALWAYS AS IDENTITY,
    scene_id INT,
    from_user_id INT,
    body TEXT,
    published TIMESTAMP,
    PRIMARY KEY(message_id),
    CONSTRAINT fk_scene
        FOREIGN KEY(scene_id)
            REFERENCES scenes(scene_id),
    CONSTRAINT fk_user
        FOREIGN KEY(from_user_id)
            REFERENCES users(user_id)
);