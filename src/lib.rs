use postgrest::Postgrest;
use serde::{Deserialize, Serialize};
use serde_json::json;

#[derive(Serialize, Default, Clone)]
pub struct MessageVm {
    pub message_id: i32,
    pub message_body: Option<String>,
    pub user_display_name: Option<String>,
    pub image_path: Option<String>,
}

#[derive(Default, Deserialize, Serialize)]
pub struct Message {
    pub message_id: i32,
    pub scene_id: Option<i32>,
    pub from_user_id: Option<i32>,
    pub body: Option<String>,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct NewMessage {
    pub scene_id: i32,
    pub from_user_id: i32,
    pub body: String,
}

#[derive(Default, Deserialize, Serialize)]
struct User {
    user_id: i32,
    display_name: Option<String>,
    email: Option<String>,
    image_path: Option<String>,
}

#[derive(Serialize, Default, Clone)]
pub struct IndexVm {
    pub messages: Vec<MessageVm>,
}

pub async fn insert_message(message: NewMessage) -> Result<(), hyper::Error> {
    let client = Postgrest::new("http://localhost:3000");
    client
        .from("messages")
        .insert(json!(message).to_string())
        .execute()
        .await
        .unwrap();

    Ok(())
}

pub async fn retrieve_messages(_scene_id: i32) -> IndexVm {
    let client = Postgrest::new("http://localhost:3000");
    let messages = client
        .from("messages")
        .select("*")
        .execute()
        .await
        .expect("got a response")
        .json::<Vec<Message>>()
        .await
        .expect("response had text");

    let user_ids = messages
        .iter()
        .map(|msg| msg.from_user_id.unwrap().to_string());

    let users = client
        .from("users")
        .in_("user_id", user_ids)
        .select("*")
        .execute()
        .await
        .expect("no problem getting users response")
        .json::<Vec<User>>()
        .await
        .expect("no problem deserializing users");

    let vms = messages
        .iter()
        .map(|msg| {
            let usr = match msg.from_user_id {
                Some(id) => users
                    .iter()
                    .find(|usr| usr.user_id == id)
                    .unwrap()
                    .to_owned(),
                None => &User {
                    user_id: 0,
                    display_name: None,
                    image_path: None,
                    email: None,
                },
            };
            MessageVm {
                message_id: msg.message_id,
                message_body: msg.body.to_owned(),
                user_display_name: usr.display_name.to_owned(),
                image_path: usr.image_path.to_owned(),
            }
        })
        .collect();

    IndexVm { messages: vms }
}
