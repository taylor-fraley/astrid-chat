extern crate hyper;
use handlebars::Handlebars;
use hyper::server::Server;
use listenfd::ListenFd;
use std::{collections::HashMap, convert::Infallible, fs, sync::Arc};
use warp::Filter;

use astrid_chat::*;

async fn render_template(
    name: &str,
    hbs: Arc<Handlebars<'_>>,
) -> Result<impl warp::Reply, Infallible> {
    let vm = retrieve_messages(1).await;
    let render = hbs.render(name, &vm).unwrap_or_else(|err| err.to_string());
    Ok(warp::reply::html(render))
}

async fn create_new_message(text: String) -> Result<(), hyper::Error> {
    let message = NewMessage {
        scene_id: 1,
        from_user_id: 1,
        body: text,
    };

    insert_message(message).await.unwrap();

    Ok(())
}

fn register_templates() -> Handlebars<'static> {
    let mut hb = Handlebars::new();
    hb.register_template_file("navbar", "./src/templates/navbar.html")
        .unwrap();
    hb.register_template_file(
        "chat_input_opened",
        "./src/templates/chat_input_opened.html",
    )
    .unwrap();
    hb.register_template_file(
        "chat_input_collapsed",
        "./src/templates/chat_input_collapsed.html",
    )
    .unwrap();
    // register the templates
    hb.register_template_file("messages", "./src/templates/messages.hbs")
        .unwrap();

    hb.register_template_file("index", "./src/templates/index.hbs")
        .unwrap();

    hb
}

#[tokio::main]
async fn main() {
    let hb = register_templates();
    let hb = Arc::new(hb);
    let handlebars = move |template: &'static str| render_template(template, hb.clone());

    let post_messages = warp::post()
        .and(warp::path("messages"))
        .and(warp::body::form())
        .and_then(|msg: HashMap<String, String>| async move {
            let result = create_new_message(msg["msg"].to_owned()).await;
            match result {
                Ok(()) => Ok(format!("store worked!")),
                Err(_) => Err(warp::reject::reject()),
            }
        })
        .map(|_| "messages")
        .and_then(handlebars.clone());

    let index = warp::get()
        .and(warp::path::end())
        .map(|| "index")
        .and_then(handlebars.clone());

    let get_msgs = warp::get()
        .and(warp::path("messages"))
        .map(|| "messages")
        .and_then(handlebars.clone());

    let get_chat_input_opened = warp::get().and(warp::path("chat-input-opened")).map(|| {
        warp::reply::html(fs::read_to_string("src/templates/chat_input_opened.html").unwrap())
    });

    let get_chat_input_collapsed = warp::get().and(warp::path("chat-input-collapsed")).map(|| {
        warp::reply::html(fs::read_to_string("src/templates/chat_input_collapsed.html").unwrap())
    });

    let routes = warp::get()
        .and(index)
        .or(get_chat_input_opened)
        .or(get_msgs)
        .or(get_chat_input_collapsed)
        .or(post_messages);
    //warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;

    // hyper let's us build a server from a TcpListener (which will be
    // useful shortly). Thus, we'll need to convert our `warp::Filter` into
    // a `hyper::service::MakeService` for use with a `hyper::server::Server`.
    let svc = warp::service(routes);

    let make_svc = hyper::service::make_service_fn(|_: _| {
        // the clone is there because not all warp filters impl Copy
        let svc = svc.clone();
        async move { Ok::<_, Infallible>(svc) }
    });

    let mut listenfd = ListenFd::from_env();
    // if listenfd doesn't take a TcpListener (i.e. we're not running via
    // the command above), we fall back to explicitly binding to a given
    // host:port.
    let server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        Server::from_tcp(l).unwrap()
    } else {
        Server::bind(&([127, 0, 0, 1], 3030).into())
        //Server::bind(&([192, 168, 1, 12], 3030).into())
    };

    server.serve(make_svc).await.unwrap();
}
